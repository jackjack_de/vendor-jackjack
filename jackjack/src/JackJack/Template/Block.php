<?php


/**
 * Description of Block
 *
 * @author stefan
 */

namespace JackJack\Template;

class Block extends \Prefab {

    /**
     * Retrieve all block names
     *
     * @var array
     */
    protected static $_blockCache = array();

    /**
     * Render the block class
     *
     * @param array $node
     * @return string
     */
    static public function render(array $node) {

        $attr = $node['@attrib'];

        $type = \Template::instance()->token($attr['type']);
        $name = \Template::instance()->token($attr['name']);
        $template = \Template::instance()->token($attr['template']);

        /**
         * if block allready cached
         */
        if (isset(self::$_blockCache[$name])) {
            if (is_object(self::$_blockCache[$name])) {

                /**
                 * Return cached block
                 */
                return self::$_blockCache[$name];
            }
        }

        /**
         * get name of block class
         */
        $blockClassName = self::getClassName($type);

        if (class_exists($blockClassName, true)) {
            $blockClass = new $blockClassName($attr);

            return $blockClass;
        }

        return null;
    }

    protected static function getClassName($type) {
        $type = ucwords($type, '/');
        $type = str_replace('/', '\\', $type);

        return '\\Block\\' . $type;
    }

}
